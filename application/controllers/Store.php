<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
    }
	
	public function index()
	{
		$this->load->model('product_model');
		
		//get data from the database
		$data['products'] = $this->product_model->get_products();
		$data['action'] = "cart";
		$this->load->view('layout/header', $data);
		$this->load->view('store/index', $data);
		$this->load->view('layout/footer');
	}
	
	public function warehouse()
	{
		$this->load->model('product_model');
		$data['action'] = "warehouse";
		$this->load->view('layout/header',$data);
		$this->load->view('store/warehouse', $data);
		$this->load->view('layout/footer');
	}
	
}
