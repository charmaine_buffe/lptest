<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('img_url'))
{
    function img_url($file)
    {
        return base_url().'assets/images/'.$file;
    }
}
if ( ! function_exists('css_url'))
{
    function css_url($file)
    {
        return base_url().'assets/css/'.$file;
    }
}
if ( ! function_exists('js_url'))
{
    function js_url($file)
    {
        return base_url().'assets/js/'.$file;
    }
}