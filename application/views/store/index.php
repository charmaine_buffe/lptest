<div class="container">
	<div class="row grid">
		<?php foreach($products as $product){ ?>
		<div class="col-md-2 col-sm-4 col-xs-4 grid-item">
			<div class="item-container">
				<div class="product-image">
					<img src="<?php echo $product['image']; ?>" />
				</div>
				<div class="product-description">
					<div class="product-name">
						<span><?php echo $product['name'];?></span>
					</div>
					<div class="price-block">
						<?php if($product['count'] > 0){ ?>
							&#8369;	<span class="item-price" id="price<?php echo $product['id']; ?>"><?php echo $product['price']; ?></span>
						<?php }else{ ?>
							&#8369; <span class="item-price noaavail" id="price<?php echo $product['id']; ?>"><?php echo $product['price']; ?></span>
						<?php } ?>
						
					</div>
				</div>
				<div class="atcbutton">
					<span class="addtocart" data-id="<?php echo $product['id']; ?>" data-toggle="modal" data-target="#addcartModal">Add to Cart</span>
                </div>
				<!-- <button type="button" class="btn btn-success addtocart" data-id="<?php echo $product['id']; ?>" data-toggle="modal" data-target="#addcartModal">Add to Cart</button> -->
			</div>
		 </div>
		<?php } ?>
	</div>
	<div class="modal fade" tabindex="-1" role="dialog" id="addcartModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modallabel">Add to Cart</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<form action="" method="POST" id="cartform">
								<div class="form-group">
								    <label for="name">Name</label>
								    <input type="text" class="form-control" id="item-name" placeholder="Name" name="item-name">
							  	</div>
							  	<div class="form-group">
								    <label for="code">Code</label>
								    <input type="text" class="form-control" id="item-count" placeholder="Code" name="item-count">
							  	</div>
							  	<button type="submit" class="btn btn-default" name="unitform">Submit</button>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>	
</div>
<script>
	$(document).ready(function(){
	    function addtocart(){
	    	
	    }
		$(".addtocart").click(function(){
			
		});
	});
	$('.grid').masonry({
	  // options
	  itemSelector: '.grid-item',
	  columnWidth: '.grid-item',
	});
</script>