<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>LocalPhone Test</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo img_url('store-icon.png'); ?>">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo css_url('bootstrap.min.css'); ?>" >
	<link rel="stylesheet" type="text/css" href="<?php echo css_url('main.css'); ?>" >
	<script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url(); ?>">MyStore</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php if($action == 'cart') echo 'active'; ?>"><a href="<?php echo base_url('store'); ?>">Store <span class="sr-only">(current)</span></a></li>
        <li class="<?php if($action == 'warehouse') echo 'active'; ?>"><a href="<?php echo base_url('store/warehouse'); ?>">Warehouse</a></li>
      </ul>
      <?php if($action == 'cart'){ ?>
      	<ul class="nav navbar-nav navbar-right">
      		<li><a href="#" class="cart-info">Cart <span class="badge">42</span></a></li>
      	</ul>
      <?php } ?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>