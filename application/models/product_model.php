<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_Model extends CI_Model {
	public function __construct()
    {
        parent::__construct();
    }
	function get_products()
	{
		$query = $this->db->get('products');
		if($query->num_rows() > 0){
			$result = $query->result_array();
			return $result;
		}else{
			return false;
		}
	}
}
