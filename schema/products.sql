-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2016 at 05:30 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(76) DEFAULT NULL,
  `description` varchar(295) DEFAULT NULL,
  `price` varchar(97) DEFAULT NULL,
  `count` int(4) DEFAULT NULL,
  `image` varchar(1314) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `count`, `image`) VALUES
(1, 'Furosemide', 'Morbi quis tortor id nulla ultrices aliquet.', '59.26', 165, 'https://robohash.org/quinihilexpedita.png?size=128x128&set=set1'),
(2, 'Anti-Bacterial Hand', 'Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.', '77.11', 432, 'https://robohash.org/sedassumendaquibusdam.jpg?size=128x128&set=set1\n'),
(3, 'Pollens - Trees, Elm, American Ulmus americana', 'In congue.', '15.76', 386, 'https://robohash.org/errorquiquam.jpg?size=128x128&set=set1\n'),
(4, 'Equate Cleansing Pads', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.', '30.68', 181, 'https://robohash.org/placeatoptiocorrupti.jpg?size=128x128&set=set1\n'),
(5, 'CARE ONE', 'Vestibulum ac est lacinia nisi venenatis tristique.', '70', 438, 'https://robohash.org/veniamnonrecusandae.jpg?size=128x128&set=set1\n'),
(6, 'Pussy Willow', 'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '53.47', 164, 'https://robohash.org/dictahicamet.jpg?size=128x128&set=set1\n'),
(7, 'Fentanyl Citrate, Bupivacaine HCl', 'Aenean lectus.', '78.57', 210, 'https://robohash.org/quifugitsuscipit.png?size=128x128&set=set1\n'),
(8, 'Minocycline Hydrochloride', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '40.96', 199, 'https://robohash.org/dolorumametaut.png?size=128x128&set=set1\n'),
(9, 'ropinirole hydrochloride', 'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.', '21.11', 315, 'https://robohash.org/laboriosamnobisqui.jpg?size=128x128&set=set1\n'),
(10, 'Omeprazole', 'Duis aliquam convallis nunc.', '87.94', 109, 'https://robohash.org/omnisnihilmaxime.bmp?size=128x128&set=set1\n'),
(11, 'healthy accents stool softener', 'Etiam vel augue. Vestibulum rutrum rutrum neque.', '67.21', 100, 'https://robohash.org/officiasedat.bmp?size=128x128&set=set1\n'),
(12, 'Procardia', 'Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante.', '71.83', 405, 'https://robohash.org/dignissimosetad.bmp?size=256x256&set=set1'),
(13, 'Ceftriaxone', 'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.', '89.41', 178, 'https://robohash.org/numquamautsunt.jpg?size=256x256&set=set1'),
(14, 'DIPHENHYDRAMINE HYDROCHLORIDE', 'Mauris sit amet eros.', '73.31', 278, 'https://robohash.org/easintsuscipit.bmp?size=256x256&set=set1'),
(15, 'Fenofibrate', 'Aenean auctor gravida sem.', '35.42', 335, 'https://robohash.org/delenitinecessitatibusid.bmp?size=256x256&set=set1'),
(16, 'Hectorol', 'Donec posuere metus vitae ipsum. Aliquam non mauris.', '81.2', 489, 'https://robohash.org/ametillolabore.jpg?size=256x256&set=set1'),
(17, 'LBEL divine lip gloss SPF 15', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.', '95.46', 107, 'https://robohash.org/fugitmolestiaenisi.jpg?size=256x256&set=set1'),
(18, 'Septicare', 'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.', '47.31', 477, 'https://robohash.org/aspernaturdoloremvoluptatem.png?size=256x256&set=set1'),
(19, '60-Second Fluoride', 'Phasellus id sapien in sapien iaculis congue.', '44.01', 279, 'https://robohash.org/temporenullaea.png?size=256x256&set=set1'),
(20, 'Ranitidine - Acid Reducer', 'Aenean sit amet justo. Morbi ut odio.', '57.01', 132, 'https://robohash.org/fugitducimussapiente.jpg?size=256x256&set=set1'),
(21, 'Lancome Paris BB Bienfait Teinte Beauty Balm Broad Spectrum SPF 30 Sunscreen', 'In eleifend quam a odio.', '77.26', 219, 'https://robohash.org/fugitmodienim.bmp?size=256x256&set=set1'),
(22, 'Original Formula Motion Sickness Relief', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '69.16', 165, 'https://robohash.org/officiaaliquideius.jpg?size=256x256&set=set1'),
(23, 'Prenatal Mega Antioxidant', 'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.', '45.35', 163, 'https://robohash.org/estsuscipitvoluptates.bmp?size=256x256&set=set1'),
(24, 'Lamotrigine', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus.', '31.77', 342, 'https://robohash.org/reiciendisexcepturidolorum.png?size=256x256&set=set1'),
(25, 'MultiHance', 'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', '24.51', 201, 'https://robohash.org/eiussitet.png?size=256x256&set=set1'),
(26, 'Shimmery Sun', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.', '41.39', 341, 'https://robohash.org/aliquamearumsint.bmp?size=128x128&set=set1\n'),
(27, 'NP Thyroid 90', 'Maecenas tincidunt lacus at velit.', '53.14', 239, 'https://robohash.org/quasvoluptassunt.bmp?size=128x128&set=set1\n'),
(28, 'Lidocaine Hydrochloride', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.', '91.81', 458, 'https://robohash.org/quibusdamfacereratione.jpg?size=128x128&set=set1\n'),
(29, 'GLYCOPYRROLATE', 'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '74.87', 466, 'https://robohash.org/magnivoluptatemrepudiandae.bmp?size=128x128&set=set1\n'),
(30, 'PLEO THYM', 'Etiam vel augue.', '63.86', 165, 'https://robohash.org/estcumquenisi.bmp?size=128x128&set=set1\n'),
(31, 'Metronidazole', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae', '10.00', 58, 'https://robohash.org/delectusnullaaut.jpg?size=128x128&set=set1\n'),
(32, 'Valproic Acid', 'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '56.91', 263, 'https://robohash.org/nonculpaet.png?size=128x128&set=set1\n'),
(33, 'Hydromorphone Hydrochloride', 'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', '21.79', 198, 'https://robohash.org/estdoloresperferendis.png?size=128x128&set=set1\n'),
(34, 'Dexamethasone Sodium Phosphate', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.', '17.12', 295, 'https://robohash.org/noncorruptiratione.jpg?size=128x128&set=set1\n'),
(35, 'UVA/UVB SPF 15', 'Vivamus tortor. Duis mattis egestas metus.', '27.86', 345, 'https://robohash.org/quiadignissimostenetur.png?size=128x128&set=set1\n'),
(36, 'Ibuprofen PM', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '56.21', 455, 'https://robohash.org/temporibusharumlibero.jpg?size=128x128&set=set1\n'),
(37, 'TISSEEL', 'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.', '94.51', 144, 'https://robohash.org/consequaturcorporisquidem.bmp?size=128x128&set=set1\n'),
(38, 'SIMPLY RIGHT BODY CARE', 'Vivamus in felis eu sapien cursus vestibulum.', '60.13', 145, 'https://robohash.org/recusandaetemporibusqui.png?size=128x128&set=set1\n'),
(39, 'ULTRA HYDRA SERUM', 'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.', '26.71', 346, 'https://robohash.org/temporevelitaut.png?size=128x128&set=set1\n'),
(40, 'Amlodipine besylate and Atorvastatin calcium', 'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.', '85.28', 314, 'https://robohash.org/seddoloresit.png?size=128x128&set=set1\n'),
(41, 'Metronidazole', 'Morbi vel lectus in quam fringilla rhoncus.', '23.5', 447, 'https://robohash.org/etdignissimoseum.jpg?size=128x128&set=set1\n'),
(42, 'Ofloxacin', 'In hac habitasse platea dictumst.', '91.26', 281, 'https://robohash.org/explaceatrecusandae.jpg?size=256x256&set=set1'),
(43, 'Dry Idea A.Dry Antiperspirant Solid Unscented', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '84.84', 424, 'https://robohash.org/repellatinat.jpg?size=256x256&set=set1'),
(44, 'KARAYA GUM', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '23.26', 472, 'https://robohash.org/autemquoveniam.bmp?size=256x256&set=set1'),
(45, 'Argentum 8', 'Aliquam sit amet diam in magna bibendum imperdiet.', '51.5', 274, 'https://robohash.org/rerumoccaecatiquia.bmp?size=256x256&set=set1'),
(46, 'Levetiracetam', 'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.', '75.36', 386, 'https://robohash.org/eaquererumnihil.bmp?size=256x256&set=set1'),
(47, 'Levaquin', 'Vestibulum ac est lacinia nisi venenatis tristique.', '57.84', 428, 'https://robohash.org/omnismagnamin.png?size=256x256&set=set1'),
(48, 'Carbidopa and Levodopa', 'Vestibulum sed magna at nunc commodo placerat. Praesent blandit.', '19.1', 464, 'https://robohash.org/dignissimoseosexercitationem.bmp?size=256x256&set=set1'),
(49, 'Spring Grove Unscented Hand Sanitizer', 'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', '77.15', 481, 'https://robohash.org/veliteosmaxime.jpg?size=256x256&set=set1'),
(50, 'Hawaiian Tropic Carrot Protective Oil SPF 30', 'Aenean sit amet justo. Morbi ut odio.', '46.15', 500, 'https://robohash.org/ipsaesseest.png?size=256x256&set=set1'),
(51, 'Salt Grass', 'Nulla facilisi.', '74.13', 250, 'https://robohash.org/autvoluptatemut.bmp?size=256x256&set=set1'),
(52, 'Rowasa', 'Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '54.93', 141, 'https://robohash.org/ducimusquianatus.bmp?size=256x256&set=set1'),
(53, 'esika', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '24.84', 171, 'https://robohash.org/autautemrepellat.png?size=256x256&set=set1'),
(54, 'Governing Vessel Conception Vessel', 'Maecenas ut massa quis augue luctus tincidunt.', '95.62', 421, 'https://robohash.org/providentconsecteturet.jpg?size=256x256&set=set1'),
(55, 'ARGEMONE MEXICANA', 'Pellentesque ultrices mattis odio. Donec vitae nisi.', '43.25', 298, 'https://robohash.org/nequepossimusaut.bmp?size=256x256&set=set1'),
(56, 'Nitrous Oxide', 'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', '31.1', 238, 'https://robohash.org/nobisestfacilis.jpg?size=256x256&set=set1'),
(57, 'Indomethacin', 'In eleifend quam a odio. In hac habitasse platea dictumst.', '93.61', 448, 'https://robohash.org/rerumfugaconsequatur.png?size=256x256&set=set1'),
(58, 'Vivelle Dot', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna.', '63.35', 463, 'https://robohash.org/voluptatemmolestiasaliquam.jpg?size=256x256&set=set1'),
(59, 'Risperidone', 'Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.', '55.6', 268, 'https://robohash.org/illumconsequaturcum.png?size=256x256&set=set1'),
(60, 'Erythromycin', 'Donec ut dolor.', '42.09', 219, 'https://robohash.org/delenitiipsaconsequuntur.bmp?size=256x256&set=set1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
